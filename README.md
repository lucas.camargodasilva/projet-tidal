# Projet Tidal, Modelica model of the Rance tidal power plant

The Projet Tidal is a simplified model of the Rance tidal and of its context.

# Authors:
* Ludovic Ausseur
* Aurora Luigia Teresa Bertini
* Lucas Camargo da Silva

# The model is composed by a main package ('Tidal'), including:
* the reservoir model 'Bassin'
* the sea model 'Mer'
* the model of the tidal 'Barrage'
* the test of the global model 'Usine test'
* the subpackage 'Components', with the main components of the Tidal ('Turbine', 'Vanne', 'Level sensor' and Control') and the components used for testing other components ('TurbineTester', 'VanneTester' and 'WaterSource')
* the subpackage 'Interfaces', with the custom connector 'WaterflowPort' and the partial models 'Waterbody' and 'Mechanical'
* the subpackage 'Tests', with standalone tests of the main components ('TestBassin', 'TestBassinMer', 'TestTurbine', 'TestVannes', 'TestControl' 

The project is hosted in the repository https://gitlab-student.centralesupelec.fr/lucas.camargodasilva/projet-tidal

# Design principles
Each component and subcomponent of the model has
* an icon to easily recognise the elements and their function inside the model
* a diagram to graphically show the connectors, the eventual subcomponents and their links
* a text (the code) with the declaration of parameters and variables (with their unit of measure and DisplayUnit when needed) and the equations with the necessary comments to help the understanding of the code.

# Operating instructions
Once the model has been opened, it is possible to observe how the components and subcomponents have been realised, as well as the interfaces and inheritance of the project (see the package 'Interfaces'). 
To see the results produced by the model, it is only necessary to simulate the 'UsineTest' model. This model simulates the Simple "Effet cycle" by default. If you want to simulate the "Double Effet" cycle, you have to change the 'Barrage' parameter 'DoubleEffet' to 'true' and set the amplitude of the tides 'mer.A' to 6m. In addition, an appropriate default duration has already been set (annotation(experiment(StopTime = 345600)).
Finally, it is possible to run the stand-alone tests contained in the package 'Tests' if a deeper comprehension of the single components is desired.