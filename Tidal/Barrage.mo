within Tidal;

model Barrage "Model of the power plant (24 turbines - 6 gates - level sensor - controller)"
  parameter Boolean DoubleEffetCycle = false "Indicate whether the cycle is 'Simple Effet' or 'Double Effet'";
  parameter Real InitialState = 0 "Initial state of the control system";
  parameter Modelica.SIunits.Length Hp0 = 11 "Initial pumping height ('Simple Effet' cycle only)";
  parameter Modelica.SIunits.VolumeFlowRate Q = 100 "Operating water flow of each turbine";
  parameter Modelica.SIunits.VolumeFlowRate Qp = 50 "Pumping water flow of each turbine";
  parameter Modelica.SIunits.Power P(displayUnit="MW") = 10000000 "Maximum operating power of each turbine";
  Tidal.Components.Vanne vanne annotation(
    Placement(visible = true, transformation(origin = {-70, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-80, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine1(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-50, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine2(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-20, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine3(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {20, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine4(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {50, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine5(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {80, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine6(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-80, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine7(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-50, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine8(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-20, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine9(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {20, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine10(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {50, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine11(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {80, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine12(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-50, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine13(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {80, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine14(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {50, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine15(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-20, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine16(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {20, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine17(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-80, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine18(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {80, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine19(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-50, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine20(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {50, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine21(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-20, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine22(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {20, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine23(q_op = Q, q_pump = Qp, power_limit = P) annotation(
    Placement(visible = true, transformation(origin = {-80, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne1 annotation(
    Placement(visible = true, transformation(origin = {0, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne2 annotation(
    Placement(visible = true, transformation(origin = {70, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne3 annotation(
    Placement(visible = true, transformation(origin = {-70, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne4 annotation(
    Placement(visible = true, transformation(origin = {0, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne5 annotation(
    Placement(visible = true, transformation(origin = {70, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Interfaces.WaterflowPort bassin "Connection to the reservoir" annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.WaterflowPort mer "Connection to the sea" annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Control control(State.start = InitialState, DoubleEffet = DoubleEffetCycle, Hp = Hp0) annotation(
    Placement(visible = true, transformation(origin = {40, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.LevelSensor levelSensor annotation(
    Placement(visible = true, transformation(origin = {0, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.LevelSensor levelSensor1 annotation(
    Placement(visible = true, transformation(origin = {70, 2}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(turbine.pb, bassin) annotation(
    Line(points = {{-90, -24}, {-100, -24}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine6.pb, bassin) annotation(
    Line(points = {{-90, -46}, {-100, -46}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine17.pb, bassin) annotation(
    Line(points = {{-90, -66}, {-100, -66}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine23.pb, bassin) annotation(
    Line(points = {{-90, -88}, {-100, -88}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine19.pb, bassin) annotation(
    Line(points = {{-60, -88}, {-60, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine1.pb, bassin) annotation(
    Line(points = {{-60, -24}, {-60, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine7.pb, bassin) annotation(
    Line(points = {{-60, -46}, {-60, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine12.pb, bassin) annotation(
    Line(points = {{-60, -66}, {-60, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine2.pb, bassin) annotation(
    Line(points = {{-30, -24}, {-32, -24}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine8.pb, bassin) annotation(
    Line(points = {{-30, -46}, {-32, -46}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine21.pb, bassin) annotation(
    Line(points = {{-30, -66}, {-32, -66}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine15.pb, bassin) annotation(
    Line(points = {{-30, -88}, {-32, -88}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine13.ps, mer) annotation(
    Line(points = {{90, -88}, {100, -88}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine18.ps, mer) annotation(
    Line(points = {{90, -66}, {100, -66}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine11.ps, mer) annotation(
    Line(points = {{90, -46}, {100, -46}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine5.ps, mer) annotation(
    Line(points = {{90, -24}, {100, -24}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine14.ps, mer) annotation(
    Line(points = {{60, -88}, {60, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine20.ps, mer) annotation(
    Line(points = {{60, -66}, {60, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine10.ps, mer) annotation(
    Line(points = {{60, -46}, {60, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine4.ps, mer) annotation(
    Line(points = {{60, -24}, {60, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine22.ps, mer) annotation(
    Line(points = {{30, -88}, {30, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine16.ps, mer) annotation(
    Line(points = {{30, -66}, {30, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine9.ps, mer) annotation(
    Line(points = {{30, -46}, {30, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine3.ps, mer) annotation(
    Line(points = {{30, -24}, {30, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine15.ps, mer) annotation(
    Line(points = {{-10, -88}, {-10, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine21.ps, mer) annotation(
    Line(points = {{-10, -66}, {-10, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine8.ps, mer) annotation(
    Line(points = {{-10, -46}, {-10, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine2.ps, mer) annotation(
    Line(points = {{-10, -24}, {-10, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine19.ps, mer) annotation(
    Line(points = {{-40, -88}, {-40, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine12.ps, mer) annotation(
    Line(points = {{-40, -66}, {-40, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine7.ps, mer) annotation(
    Line(points = {{-40, -46}, {-40, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine1.ps, mer) annotation(
    Line(points = {{-40, -24}, {-40, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine23.ps, mer) annotation(
    Line(points = {{-70, -88}, {-70, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine17.ps, mer) annotation(
    Line(points = {{-70, -66}, {-70, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine6.ps, mer) annotation(
    Line(points = {{-70, -46}, {-70, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(turbine.ps, mer) annotation(
    Line(points = {{-70, -24}, {-70, -100}, {100, -100}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne3.pb, bassin) annotation(
    Line(points = {{-80, 40}, {-100, 40}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne.pb, bassin) annotation(
    Line(points = {{-80, 80}, {-100, 80}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne1.pb, bassin) annotation(
    Line(points = {{-10, 80}, {-12, 80}, {-12, 78}, {-10, 78}, {-10, 100}, {-100, 100}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne4.pb, bassin) annotation(
    Line(points = {{-10, 40}, {-10, 100}, {-100, 100}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne2.pb, bassin) annotation(
    Line(points = {{60, 80}, {60, 100}, {-100, 100}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne5.pb, bassin) annotation(
    Line(points = {{60, 40}, {60, 100}, {-100, 100}, {-100, 0}}, color = {0, 0, 255}));
  connect(vanne2.ps, mer) annotation(
    Line(points = {{80, 80}, {100, 80}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne5.ps, mer) annotation(
    Line(points = {{80, 40}, {100, 40}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne4.ps, mer) annotation(
    Line(points = {{10, 40}, {10, 20}, {100, 20}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne1.ps, mer) annotation(
    Line(points = {{10, 80}, {10, 20}, {100, 20}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne.ps, mer) annotation(
    Line(points = {{-60, 80}, {-60, 20}, {100, 20}, {100, 0}}, color = {0, 0, 255}));
  connect(vanne3.ps, mer) annotation(
    Line(points = {{-60, 40}, {-60, 20}, {100, 20}, {100, 0}}, color = {0, 0, 255}));
  connect(control.vannes, vanne.control) annotation(
    Line(points = {{40, 11}, {40, 16}, {-40, 16}, {-40, 96}, {-70, 96}, {-70, 88}}, color = {0, 0, 127}));
  connect(control.vannes, vanne2.control) annotation(
    Line(points = {{40, 11}, {40, 96}, {70, 96}, {70, 88}}, color = {0, 0, 127}));
  connect(vanne5.control, control.vannes) annotation(
    Line(points = {{70, 48}, {70, 60}, {40, 60}, {40, 11}}, color = {0, 0, 127}));
  connect(vanne3.control, control.vannes) annotation(
    Line(points = {{-70, 48}, {-70, 60}, {-40, 60}, {-40, 16}, {40, 16}, {40, 11}}, color = {0, 0, 127}));
  connect(vanne1.control, control.vannes) annotation(
    Line(points = {{0, 88}, {0, 96}, {40, 96}, {40, 11}}, color = {0, 0, 127}));
  connect(vanne4.control, control.vannes) annotation(
    Line(points = {{0, 48}, {0, 60}, {-40, 60}, {-40, 16}, {40, 16}, {40, 11}}, color = {0, 0, 127}));
  connect(control.turbines, turbine.control) annotation(
    Line(points = {{40, -7}, {40, -12}, {-80, -12}, {-80, -16}}, color = {0, 0, 127}));
  connect(levelSensor.l, control.l) annotation(
    Line(points = {{10, 2}, {32, 2}}, color = {0, 0, 127}));
  connect(levelSensor.p, bassin) annotation(
    Line(points = {{-10, 2}, {-21, 2}, {-21, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine3.pb, bassin) annotation(
    Line(points = {{10, -24}, {0, -24}, {0, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine9.pb, bassin) annotation(
    Line(points = {{10, -46}, {0, -46}, {0, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine16.pb, bassin) annotation(
    Line(points = {{10, -66}, {0, -66}, {0, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine22.pb, bassin) annotation(
    Line(points = {{10, -88}, {0, -88}, {0, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine14.pb, bassin) annotation(
    Line(points = {{40, -88}, {40, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(bassin, turbine20.pb) annotation(
    Line(points = {{-100, 0}, {-32, 0}, {-32, -10}, {40, -10}, {40, -66}}, color = {0, 0, 255}));
  connect(turbine10.pb, bassin) annotation(
    Line(points = {{40, -46}, {40, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine4.pb, bassin) annotation(
    Line(points = {{40, -24}, {40, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine13.pb, bassin) annotation(
    Line(points = {{70, -88}, {70, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine18.pb, bassin) annotation(
    Line(points = {{70, -66}, {70, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine11.pb, bassin) annotation(
    Line(points = {{70, -46}, {70, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine5.pb, bassin) annotation(
    Line(points = {{70, -24}, {70, -10}, {-32, -10}, {-32, 0}, {-100, 0}}, color = {0, 0, 255}));
  connect(turbine1.control, control.turbines) annotation(
    Line(points = {{-50, -16}, {-50, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine2.control, control.turbines) annotation(
    Line(points = {{-20, -16}, {-20, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine3.control, control.turbines) annotation(
    Line(points = {{20, -16}, {20, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine4.control, control.turbines) annotation(
    Line(points = {{50, -16}, {50, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine5.control, control.turbines) annotation(
    Line(points = {{80, -16}, {80, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine6.control, control.turbines) annotation(
    Line(points = {{-80, -38}, {-66, -38}, {-66, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine17.control, control.turbines) annotation(
    Line(points = {{-80, -58}, {-66, -58}, {-66, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine23.control, control.turbines) annotation(
    Line(points = {{-80, -80}, {-66, -80}, {-66, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine7.control, control.turbines) annotation(
    Line(points = {{-50, -38}, {-36, -38}, {-36, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine12.control, control.turbines) annotation(
    Line(points = {{-50, -58}, {-36, -58}, {-36, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine19.control, control.turbines) annotation(
    Line(points = {{-50, -80}, {-36, -80}, {-36, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine8.control, control.turbines) annotation(
    Line(points = {{-20, -38}, {-6, -38}, {-6, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine21.control, control.turbines) annotation(
    Line(points = {{-20, -58}, {-6, -58}, {-6, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine15.control, control.turbines) annotation(
    Line(points = {{-20, -80}, {-6, -80}, {-6, -10}, {40, -10}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine9.control, control.turbines) annotation(
    Line(points = {{20, -38}, {34, -38}, {34, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine16.control, control.turbines) annotation(
    Line(points = {{20, -58}, {34, -58}, {34, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine22.control, control.turbines) annotation(
    Line(points = {{20, -80}, {34, -80}, {34, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine10.control, control.turbines) annotation(
    Line(points = {{50, -38}, {64, -38}, {64, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine20.control, control.turbines) annotation(
    Line(points = {{50, -58}, {64, -58}, {64, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine14.control, control.turbines) annotation(
    Line(points = {{50, -80}, {64, -80}, {64, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine11.control, control.turbines) annotation(
    Line(points = {{80, -38}, {94, -38}, {94, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine18.control, control.turbines) annotation(
    Line(points = {{80, -58}, {94, -58}, {94, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(turbine13.control, control.turbines) annotation(
    Line(points = {{80, -80}, {94, -80}, {94, -12}, {40, -12}, {40, -6}}, color = {0, 0, 127}));
  connect(control.l_mer, levelSensor1.l) annotation(
    Line(points = {{48, 2}, {60, 2}}, color = {0, 0, 127}));
  connect(levelSensor1.p, mer) annotation(
    Line(points = {{80, 2}, {88, 2}, {88, 0}, {100, 0}}, color = {0, 0, 255}));

annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {170, 255, 255}, fillPattern = FillPattern.Sphere, extent = {{-100, -100}, {100, 100}}, radius = 25), Rectangle(origin = {100, 0},fillColor = {192, 192, 192}, fillPattern = FillPattern.Backward, extent = {{-120, 100}, {-80, -100}}), Line(origin = {-29.9989, -16.2959}, points = {{67.5, 42.3333}, {73.5, 28.3333}, {99.5, 32.3333}, {97.5, 58.3333}, {73.5, 56.333}, {79.5, 36.3333}, {93.5, 44.3333}, {83.5, 50.3333}}, smooth = Smooth.Bezier), Line(origin = {-29.9988, -116.195}, points = {{67.5, 42.3333}, {73.5, 28.3333}, {99.5, 32.3333}, {97.5, 58.3333}, {73.5, 56.333}, {79.5, 36.3333}, {93.5, 44.3333}, {83.5, 50.3333}}, smooth = Smooth.Bezier), Line(origin = {-30.3507, 25.5634}, points = {{67.5, 42.3333}, {73.5, 28.3333}, {99.5, 32.3333}, {97.5, 58.3333}, {73.5, 56.333}, {79.5, 36.3333}, {93.5, 44.3333}, {83.5, 50.3333}}, smooth = Smooth.Bezier), Line(origin = {-29.9989, -64.8386}, points = {{67.5, 42.3333}, {73.5, 28.3333}, {99.5, 32.3333}, {97.5, 58.3333}, {73.5, 56.333}, {79.5, 36.3333}, {93.5, 44.3333}, {83.5, 50.3333}}, smooth = Smooth.Bezier), Line(origin = {-156.984, 24.5081}, points = {{69.5, 50.3333}, {81.5, 26.3333}, {87.5, 56.3333}, {97.5, 32.3333}, {105.5, 36.333}, {109.5, 46.3333}, {115.5, 52.3333}, {121.5, 34.3333}}, smooth = Smooth.Bezier), Line(origin = {-155.225, -14.1854}, points = {{69.5, 50.3333}, {81.5, 26.3333}, {87.5, 56.3333}, {97.5, 32.3333}, {105.5, 36.333}, {109.5, 46.3333}, {115.5, 52.3333}, {121.5, 34.3333}}, smooth = Smooth.Bezier), Line(origin = {-153.466, -64.1351}, points = {{69.5, 50.3333}, {81.5, 26.3333}, {87.5, 56.3333}, {97.5, 32.3333}, {105.5, 36.333}, {109.5, 46.3333}, {115.5, 52.3333}, {121.5, 34.3333}}, smooth = Smooth.Bezier), Line(origin = {-154.873, -116.547}, points = {{69.5, 50.3333}, {81.5, 26.3333}, {87.5, 56.3333}, {97.5, 32.3333}, {105.5, 36.333}, {109.5, 46.3333}, {115.5, 52.3333}, {121.5, 34.3333}}, smooth = Smooth.Bezier), Text(origin = {60, 56}, lineColor = {130, 130, 130}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}));
end Barrage;
