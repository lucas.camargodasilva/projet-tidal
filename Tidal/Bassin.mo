within Tidal;

model Bassin "The reservoir of the Rance Tidal Power Station"
  extends Tidal.Interfaces.Waterbody;
equation
  (2 * 1300000 * l + 4500000) * der(l) = p.q "The dynamics of the water level in the reservoir";
end Bassin;
