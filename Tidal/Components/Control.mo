within Tidal.Components;

model Control "Component that controls the behavior of the system"
  parameter Boolean DoubleEffet = false "Indicate whether the cycle is 'Simple Effet' or 'Double Effet'";
  Real State(start = 0) "Global state of the system";
  final parameter Real tol = 0.001 "Tolerance value to make the variable State work";
  parameter Modelica.SIunits.Length Hp = 11 "Initial pumping height ('Simple Effet' cycle only)";
  Modelica.SIunits.Length H = l_mer - l "Drop height";
  Modelica.Blocks.Interfaces.RealInput l annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-80, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput vannes annotation(
    Placement(visible = true, transformation(origin = {0, 90}, extent = {{-10, -10}, {10, 10}}, rotation = -90), iconTransformation(origin = {0, 90}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealOutput turbines annotation(
    Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Blocks.Interfaces.RealInput l_mer annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180), iconTransformation(origin = {80, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
equation
  /*State Machine to control the behavior of the system*/
  /*Based on the behavior described in http://hmf.enseeiht.fr/travaux/CD0001/travaux/optsee/hym/6/fon.htm*/
  if DoubleEffet == false then // 'Simple effet' cycle
    if State > - tol and State < tol then // State 0: open gates and turbines in the 'orifice' mode
      vannes = 1;
      turbines = 0;
      if l >= Hp then
        State = 1;
      else
        State = 0;
      end if;
    elseif State > 1 - tol and State < 1 + tol then // State 1: closed gates and turbines in the 'pompe' mode
      vannes = 0;
      turbines = 1;
      if abs(H) >= 4 then
        State = 2;
      else
        State = 1;
      end if;
    else // State 2: closed gates and turbines in the direct mode
      vannes = 0;
      turbines = 2;
      if abs(H) <= 1 then
        State = 0;
      else
        State = 2;
      end if;
    end if;
  else // 'Double effet' cycle
    if State > - tol and State < tol then // State 0: closed gates and turbines in the inverse mode
      vannes = 0;
      turbines = 3;
      if H <= 2 and der(l_mer) < 0 then
        State = 1;
      else
        State = 0;
      end if;
    elseif State > 1 - tol and State < 1 + tol then // State 1: open gates and turbines in the inverse mode
      vannes = 1;
      turbines = 3;
      if H <= 1 and der(l_mer) < 0 then
        State = 2;
      else
        State = 1;
      end if;
    elseif State > 2 - tol and State < 2 + tol then // State 2: closed gates and turbines in the 'pompe' mode
      vannes = 0;
      turbines = 1;
      if H <= -3 and der(l_mer) < 0 then
        State = 3;
      else
        State = 2;
      end if;
    else // State 3: closed gates and turbines in the direct mode
      vannes = 0;
      turbines = 2;
      if H >= -1 and der(l_mer) > 0 then
        State = 0;
      else
        State = 3;
      end if;
    end if;
  end if;

annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {158, 56}, lineColor = {130, 130, 130}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}),
    Diagram(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {0, -0.313433}, points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {158, 56}, lineColor = {130, 130, 130}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}));
end Control;
