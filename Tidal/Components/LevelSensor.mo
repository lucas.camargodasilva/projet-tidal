within Tidal.Components;

model LevelSensor "Water level sensor in meters"
  Modelica.Blocks.Interfaces.RealOutput l(unit="m")
  "Water level as the output signal"
  annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {102, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.WaterflowPort p annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  /*Define the connections of the sensor*/
  p.q = 0;
  l = p.l;
annotation(
    Diagram(graphics = {Rectangle(origin = {0, -12}, lineColor = {0, 85, 127}, fillColor = {0, 85, 127}, fillPattern = FillPattern.Solid, extent = {{-12, 40}, {12, -68}}), Polygon(lineThickness = 0.5, points = {{-12, -80}, {-12, 80}, {-10, 86}, {-6, 88}, {0, 90}, {6, 88}, {10, 86}, {12, 80}, {12, -80}, {-12, -80}}), Line(points = {{-40, -20}, {-12, -20}}), Line(points = {{-40, 20}, {-12, 20}}), Line(points = {{-40, 60}, {-12, 60}}), Line(origin = {-104.271, 0}, points = {{12, 0}, {90, 0}}, color = {0, 0, 255}), Line(points = {{12, 0}, {90, 0}}, color = {0, 170, 0}), Text(extent = {{102, -28}, {60, -78}}, textString = "m")}),
    Icon(graphics = {Rectangle(origin = {0, -12}, lineColor = {0, 85, 127}, fillColor = {0, 85, 127}, fillPattern = FillPattern.Solid, extent = {{-12, 40}, {12, -68}}), Polygon(lineThickness = 0.5, points = {{-12, -80}, {-12, 80}, {-10, 86}, {-6, 88}, {0, 90}, {6, 88}, {10, 86}, {12, 80}, {12, -80}, {-12, -80}}), Line(points = {{-40, -20}, {-12, -20}}), Line(points = {{-40, 20}, {-12, 20}}), Line(points = {{-40, 60}, {-12, 60}}), Line(origin = {-104.271, 0}, points = {{12, 0}, {90, 0}}, color = {0, 0, 255}), Line(points = {{12, 0}, {90, 0}}, color = {0, 170, 0}), Text(extent = {{126, -20}, {26, -120}}, textString = "m"), Text(lineColor = {0, 0, 255}, extent = {{-150, 130}, {150, 90}}, textString = "%name")}));
end LevelSensor;
