within Tidal.Components;

model Turbine "Model of a single turbine"
  extends Tidal.Interfaces.Mechanical;
  parameter Modelica.SIunits.VolumeFlowRate q_op = 100 "Operating flow";
  parameter Modelica.SIunits.VolumeFlowRate q_pump = 50 "Pumping flow";
  parameter Modelica.SIunits.Power power_limit(displayUnit="MW") = 10000000 "Maximum operating power";
  final parameter Real alfa_d (unit="m^(5/2)/s")=1820 "Direct flow direction constant";
  final parameter Real alfa_i (unit="m^(5/2)/s")=1420 "Inverse flow direction constant";
  final parameter Modelica.SIunits.Density rho=998.2 "Water density";
  final parameter Modelica.SIunits.Acceleration g=9.81 "Gravity acceleration";
  final parameter Real tol = 0.001 "Tolerance value to make the control work";
  Modelica.SIunits.Power P(displayUnit="MW") "Produced power";
  Modelica.Blocks.Interfaces.RealInput control annotation(
    Placement(visible = true, transformation(origin = {0, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 80}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
equation
  /*Define the water flow in the possible turbine's working modes*/
  if control >= 0 - tol and control <= 0 + tol then // "Orifice" mode
    if H > 0 then // Inverse sense
      Q=alfa_d * sqrt(abs(H));
    else // Direct sense
      Q=-alfa_i * sqrt(abs(H));
    end if;
    P = 0;
  elseif control >= 1 - tol and control <= 1 + tol then // "Pompe" mode
    if rho * g * abs(H) * q_pump < power_limit then
      Q = q_pump;
      P = rho * g * H * Q;
    else
      P = -power_limit;
      P = rho * g * H * Q;
    end if;
  elseif control >= 2 - tol and control <= 2 + tol then // Direct sense mode
    if rho * g * abs(H) * q_op < power_limit then
      Q = -q_op;
      P = rho * g * H * Q;
    else
      P = power_limit;
      P = rho * g * H * Q;
    end if;
  else // Inverse sense mode
    if rho * g * abs(H) * q_op < power_limit then
      Q = q_op;
      P = rho * g * H * Q;
    else
      P = power_limit;
      P = rho * g * H * Q;
    end if;
  end if; 

annotation(
    Icon(graphics = {Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-10, -50}, {10, 50}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-30, 10}, {-10, 50}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{10, -50}, {30, -10}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -10}, {100, 10}})}),
    Diagram(graphics = {Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-10, -50}, {10, 50}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-30, 10}, {-10, 50}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{10, -50}, {30, -10}}), Rectangle(lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -10}, {100, 10}})}));
end Turbine;
