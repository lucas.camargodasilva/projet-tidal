within Tidal.Components;

model TurbineTester "Component to test the turbine model"
  Real State(start = 0) "Global state of the system";
  final parameter Real tol = 0.001 "Tolerance value to make the variable State work";
  Modelica.Blocks.Interfaces.RealOutput turbines annotation(
    Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Blocks.Interfaces.RealInput l annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-80, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
equation
  /*Sequence of operations to test all the possible states of the turbine*/
  if State > - tol and State < tol then
    turbines = 3;
    if l >= 2 then
      State = 1;
    else
      State = 0;
    end if;
  elseif State > 1 - tol and State < 1 + tol then
    turbines = 0;
    if l > 4 - tol and l < 4 + tol then
      State = 2;
    else
      State = 1;
    end if;
  elseif State > 2 - tol and State < 2 + tol then
    turbines = 1;
    if l >= 6 then
      State = 3;
    else
      State = 2;
    end if;
  else
    turbines = 2;
    if l > 4 - tol and l < 4 + tol  then
      State = 2;
    else
      State = 3;
    end if;
  end if;
annotation(
    Diagram(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {0, -0.313433}, points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {0, -3}, extent = {{-76, 69}, {76, -69}}, textString = "TT", textStyle = {TextStyle.Bold})}),
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {0, -3}, extent = {{-76, 69}, {76, -69}}, textString = "TT", textStyle = {TextStyle.Bold})}));
end TurbineTester;
