within Tidal.Components;

model Vanne "Model of a single gate"
  extends Tidal.Interfaces.Mechanical;
  parameter Real b (unit="s/(m^2)")= 3.37798e-2  "Beta parameter in the open mode";
  final parameter Real tol = 0.001 "Tolerance value to make the control work";
  Modelica.Blocks.Interfaces.RealInput control annotation(
    Placement(visible = true, transformation(origin = {0, 100}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 80}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
equation
  /*Define the water flow passing through the gate*/
  if control >= 1 - tol and control <= 1 + tol then // Open mode
  H = b*Q/4;
  else // Closed mode
  Q = 0;
  end if;
annotation(
    Diagram(graphics = {Polygon(fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, points = {{-100, 50}, {100, -50}, {100, 50}, {0, 0}, {-100, -50}, {-100, 50}}), Line(origin = {0.364583, 30.3646}, points = {{0, -30}, {0, 10}}), Rectangle(origin = {0, 46}, fillPattern = FillPattern.Solid, extent = {{-22, 6}, {22, -6}})}),
    Icon(graphics = {Polygon(points = {{-100, 50}, {100, -50}, {100, 50}, {0, 0}, {-100, -50}, {-100, 50}}), Line(origin = {0.364583, 30.3646}, points = {{0, -30}, {0, 10}}), Rectangle(origin = {0, 46}, fillPattern = FillPattern.Solid, extent = {{-22, 6}, {22, -6}})}));
end Vanne;
