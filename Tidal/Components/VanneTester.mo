within Tidal.Components;

model VanneTester "Component to test the gate model"
  Modelica.Blocks.Interfaces.RealOutput vannes annotation(
    Placement(visible = true, transformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {0, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Blocks.Interfaces.RealInput l annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-80, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
equation
  /*Test the two possible states of the gate (open and closed)*/
  vannes = if rem(time,10000) < 5000 then 0 else 1;
annotation(
    Diagram(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {0, -3}, extent = {{-76, 69}, {76, -69}}, textString = "VT", textStyle = {TextStyle.Bold})}),
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-84, 0}, {-54, 0}, {-54, 40}, {-24, 40}, {-24, -70}, {6, -70}, {6, 80}, {36, 80}, {36, -20}, {66, -20}, {66, 60}}), Text(origin = {0, -3}, extent = {{-76, 69}, {76, -69}}, textString = "VT", textStyle = {TextStyle.Bold})}));
end VanneTester;
