within Tidal.Components;

model WaterSource "Water flow source"
  parameter Modelica.SIunits.VolumeFlowRate Q = 100000 "Water flow";
  extends Tidal.Interfaces.Waterbody;
equation
  /*Define the flow at the port*/
  p.q = -Q; // The flow is negative because it is going out of the block
end WaterSource;
