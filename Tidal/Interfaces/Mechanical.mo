within Tidal.Interfaces;

partial model Mechanical "Component with two ports, pb and ps, at different heights and a water flow from ps to pb"
  Modelica.SIunits.Length H "Drop height";
  Modelica.SIunits.VolumeFlowRate Q "Water flow";
  Interfaces.WaterflowPort pb "Connection to the reservoir" annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.WaterflowPort ps "Connection to the sea" annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  /*Equality of the flows*/
  ps.q + pb.q = 0;
  /*Define the drop height*/
  H = ps.l - pb.l;
  /*Define the water flow*/
  Q = ps.q;  
end Mechanical;
