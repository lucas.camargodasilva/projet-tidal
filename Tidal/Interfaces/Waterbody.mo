within Tidal.Interfaces;

partial model Waterbody "Waterbody with a water level, a water flow rate and one waterflow port"
  Modelica.SIunits.Height l(start = 7) "Level of the waterbody";
  Interfaces.WaterflowPort p(l(start=0)) "Waterbody connection" annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  /*Define the connection of the waterbody*/
  l = p.l;
annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {170, 255, 255}, fillPattern = FillPattern.Sphere, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {-44.7727, 17.1212}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Line(origin = {-45.2273, -32.4242}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Line(origin = {-46.1364, -93.3333}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Text(origin = {56, -218}, lineColor = {0, 0, 255}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}),
    Diagram(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {170, 255, 255}, fillPattern = FillPattern.Sphere, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {-44.7727, 17.1212}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Line(origin = {-45.2273, -32.4242}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Line(origin = {-46.1364, -93.3333}, points = {{-32.5, 44.3333}, {-2.5, 8.3333}, {3.5, 56.3333}, {27.5, 10.3333}, {51.5, 54.333}, {79.5, 12.3333}, {93.5, 50.3333}, {123.5, 22.3333}}, smooth = Smooth.Bezier), Text(origin = {56, -218}, lineColor = {0, 0, 255}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}));
end Waterbody;
