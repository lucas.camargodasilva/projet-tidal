within Tidal.Interfaces;

connector WaterflowPort "Water connection of a waterbody"
  Modelica.SIunits.Length l "Water level";
  flow Modelica.SIunits.VolumeFlowRate q "Water flowing through the connection";
  annotation(defaultComponentName="waterFlowPort_p",
    Diagram(graphics = {Rectangle(lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-40, 40}, {40, -40}}), Text(lineColor = {0, 0, 255}, extent = {{-160, 110}, {40, 50}}, textString = "%name")}),
    Icon(graphics = {Rectangle(lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}})}));
end WaterflowPort;
