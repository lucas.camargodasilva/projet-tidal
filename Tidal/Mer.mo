within Tidal;

model Mer "The sea and its tides"
  extends Tidal.Interfaces.Waterbody;
  parameter Modelica.SIunits.Length A = 5 "Amplitude of the tides";
  parameter Modelica.SIunits.Length m = 7 "Mean height of the tides";
equation
  l = m + A*Modelica.Math.sin((2*3.14/(60*60*24))*time); // Generate sinusoidal tides
end Mer;
