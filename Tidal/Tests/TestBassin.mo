within Tidal.Tests;

model TestBassin "Standalone test of the reservoir"
  Tidal.Bassin bassin annotation(
    Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Tidal.Components.WaterSource waterSource annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
equation
  connect(bassin.p, waterSource.p) annotation(
    Line(points = {{-40, 0}, {40, 0}}, color = {0, 0, 255}));
  annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}),
    experiment(StartTime = 0, StopTime = 259200, Tolerance = 1e-06, Interval = 172.8),
    Diagram(graphics = {Text(origin = {0, 57}, extent = {{-78, 19}, {78, -19}}, textString = "Bassin test"), Text(origin = {0, -55}, extent = {{-76, 9}, {76, -9}}, textString = "observe bassin.l", textStyle = {TextStyle.Italic})}));
end TestBassin;
