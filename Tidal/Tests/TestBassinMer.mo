within Tidal.Tests;

model TestBassinMer "Standalone test of the reservoir and the sea models"
  Tidal.Bassin bassin annotation(
    Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Tidal.Mer mer(A = 7) annotation(
    Placement(visible = true, transformation(origin = {60, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 180)));
equation
  connect(bassin.p, mer.p) annotation(
    Line(points = {{-40, 0}, {40, 0}}, color = {0, 0, 255}));
  annotation(
    experiment(StartTime = 0, StopTime = 259200, Tolerance = 1e-06, Interval = 172.8),
    Icon(graphics = {Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}}), Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}),
    Diagram(graphics = {Text(origin = {0, 70}, extent = {{-90, 26}, {90, -26}}, textString = "Bassin and Mer test"), Text(origin = {-1, -70}, extent = {{-95, 14}, {95, -14}}, textString = "observe bassin.l, mer.l, bassin.p.q and mer.p.q", textStyle = {TextStyle.Italic})}));
end TestBassinMer;
