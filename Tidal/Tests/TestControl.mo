within Tidal.Tests;

model TestControl "Standalone test of the control system"
  Tidal.Bassin bassin(l(start = 4))  annotation(
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Mer mer(A = 4) annotation(
    Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Tidal.Components.Control control(DoubleEffet = false, Hp = 10)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Vanne vanne(b = 4e-4)  annotation(
    Placement(visible = true, transformation(origin = {0, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.LevelSensor levelSensor annotation(
    Placement(visible = true, transformation(origin = {-30, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.Turbine turbine(power_limit = 1000000000, q_op = 4000, q_pump = 1000)  annotation(
    Placement(visible = true, transformation(origin = {0, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.LevelSensor levelSensor_mer annotation(
    Placement(visible = true, transformation(origin = {32, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(levelSensor.l, control.l) annotation(
    Line(points = {{-20, 0}, {-8, 0}}, color = {0, 0, 127}));
  connect(levelSensor.p, bassin.p) annotation(
    Line(points = {{-40, 0}, {-60, 0}}, color = {0, 0, 255}));
  connect(control.vannes, vanne.control) annotation(
    Line(points = {{0, 9}, {0, 48}}, color = {0, 0, 127}));
  connect(turbine.control, control.turbines) annotation(
    Line(points = {{0, -32}, {0, -9}}, color = {0, 0, 127}));
  connect(vanne.pb, bassin.p) annotation(
    Line(points = {{-10, 40}, {-50, 40}, {-50, 0}, {-60, 0}}, color = {0, 0, 255}));
  connect(turbine.pb, bassin.p) annotation(
    Line(points = {{-10, -40}, {-50, -40}, {-50, 0}, {-60, 0}}, color = {0, 0, 255}));
  connect(turbine.ps, mer.p) annotation(
    Line(points = {{10, -40}, {50, -40}, {50, 0}, {60, 0}}, color = {0, 0, 255}));
  connect(vanne.ps, mer.p) annotation(
    Line(points = {{10, 40}, {50, 40}, {50, 0}, {60, 0}}, color = {0, 0, 255}));
  connect(control.l_mer, levelSensor_mer.l) annotation(
    Line(points = {{8, 0}, {22, 0}}, color = {0, 0, 127}));
  connect(levelSensor_mer.p, mer.p) annotation(
    Line(points = {{42, 0}, {60, 0}}, color = {0, 0, 255}));
  annotation(
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}),
    experiment(StartTime = 0, StopTime = 345600, Tolerance = 1e-06, Interval = 866.165),
  Diagram(graphics = {Text(origin = {-1, 72}, extent = {{-63, 12}, {63, -12}}, textString = "Control test"), Text(origin = {1, -86}, extent = {{-95, 14}, {95, -14}}, textString = "observe control.l, control.State, control.vannes and control.turbines", textStyle = {TextStyle.Italic}), Text(origin = {-1, -64}, extent = {{-41, 4}, {41, -4}}, textString = "Simple effet : mer.A = 4", textStyle = {TextStyle.Italic}), Text(origin = {-1, -74}, extent = {{-41, 4}, {41, -4}}, textString = "Double effet : mer.A = 6", textStyle = {TextStyle.Italic})}));
end TestControl;
