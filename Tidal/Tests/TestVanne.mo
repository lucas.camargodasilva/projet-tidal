within Tidal.Tests;

model TestVanne "Standalone test of the gate"
  Tidal.Bassin bassin(l(start = 0)) annotation(
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Mer mer(A = 0, l(start = 1), m = 1) annotation(
    Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Components.Vanne vanne annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.VanneTester vanneTester annotation(
    Placement(visible = true, transformation(origin = {0, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Components.LevelSensor levelSensor annotation(
    Placement(visible = true, transformation(origin = {-30, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(bassin.p, vanne.pb) annotation(
    Line(points = {{-60, 0}, {-10, 0}}, color = {0, 0, 255}));
  connect(levelSensor.p, bassin.p) annotation(
    Line(points = {{-40, 40}, {-50, 40}, {-50, 0}, {-60, 0}}, color = {0, 0, 255}));
  connect(vanneTester.vannes, vanne.control) annotation(
    Line(points = {{0, 32}, {0, 8}}, color = {0, 0, 127}));
  connect(vanne.ps, mer.p) annotation(
    Line(points = {{10, 0}, {60, 0}}, color = {0, 0, 255}));
  connect(levelSensor.l, vanneTester.l) annotation(
    Line(points = {{-20, 40}, {-8, 40}}, color = {0, 0, 127}));
  annotation(
    Diagram(graphics = {Text(origin = {-1, -50}, extent = {{-95, 14}, {95, -14}}, textString = "observe vanne.pb.l and vanne.control", textStyle = {TextStyle.Italic}), Text(origin = {0, 72}, extent = {{-68, 12}, {68, -12}}, textString = "Vanne test")}),
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}),
    experiment(StartTime = 0, StopTime = 345600, Tolerance = 1e-06, Interval = 172.886));
end TestVanne;
