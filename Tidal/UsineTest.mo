within Tidal;

model UsineTest
  Bassin bassin annotation(
    Placement(visible = true, transformation(origin = {-80, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal.Mer mer(A = 4)  annotation(
    Placement(visible = true, transformation(origin = {80, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Tidal.Barrage usineDeLaRance(DoubleEffetCycle = false, Hp0 = 10.9, Q = 150)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-40, -40}, {40, 40}}, rotation = 0)));
equation
  connect(bassin.p, usineDeLaRance.bassin) annotation(
    Line(points = {{-70, 0}, {-40, 0}}, color = {0, 0, 255}));
  connect(usineDeLaRance.mer, mer.p) annotation(
    Line(points = {{40, 0}, {70, 0}}, color = {0, 0, 255}));
  annotation(
    experiment(StartTime = 0, StopTime = 345600, Tolerance = 1e-06, Interval = 217.632),
    Icon(graphics = {Ellipse(lineColor = {75, 138, 73}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}}, endAngle = 360), Polygon(lineColor = {0, 0, 255}, fillColor = {75, 138, 73}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-36, 60}, {64, 0}, {-36, -60}, {-36, 60}})}),
  Diagram(graphics = {Text(origin = {-1, 82}, extent = {{-63, 12}, {63, -12}}, textString = "Usine test"), Text(origin = {-1, -64}, extent = {{-41, 4}, {41, -4}}, textString = "Simple effet : mer.A = 4", textStyle = {TextStyle.Italic}), Text(origin = {-1, -74}, extent = {{-41, 4}, {41, -4}}, textString = "Double effet : mer.A = 6", textStyle = {TextStyle.Italic}), Text(origin = {0, -86}, extent = {{-86, 8}, {86, -8}}, textString = "observe bassin.l, usineDeLaRance.control.State and mer.l", textStyle = {TextStyle.Italic})}));
end UsineTest;
